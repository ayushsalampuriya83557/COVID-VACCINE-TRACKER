Made an covid-vaccine tracker app. it will generate alert and send an email when vaccine slot is available based on user's pincode locality.

concepts used:
1. api call.
2. unmarshalling of json response.
3. cron job
4. sending email using github.com/acmacalister/sms
5. using config.toml file for local settings and configurations
6. error handling
