package api

import (
	"COVID-VACCINE-TRACKER/structs"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	// "strconv"

	"github.com/BurntSushi/toml"
	"github.com/acmacalister/sms"
)

func FindAvailableSlots() error {

	var config structs.Config

	_, err := toml.DecodeFile("./settings.toml", &config)
	if err != nil {
		fmt.Println(err)
		return err
	}

	fmt.Printf("%+v\n", config)

	urlStr := config.AllUrls
	emailSettings := config.EmailConfig

	urlString := urlStr.ServerUrl + urlStr.FindByPinUrl + "?pincode=842001&date=12-06-2021"
	urls, err := url.Parse(urlString)
	if err != nil {
		fmt.Println(err)
		return err
	}

	request, err := http.NewRequest("GET", urls.String(), nil)
	if err != nil {
		fmt.Println(err)
		return err
	}

	client := &http.Client{
		Timeout: 5000000000000,
	}

	response, err := client.Do(request)
	if err != nil {
		fmt.Println(err)
		return err
	}

	defer response.Body.Close()

	if response.StatusCode != 200 {
		fmt.Println(response.Body)
		return errors.New("Failed to get successful response")
	}

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}

	m := map[string]interface{}{}
	err = json.Unmarshal(data, &m)
	if err != nil {
		fmt.Println("Failed to Unmarshal total body:", err)
		return err
	}

	bodybytes, err := json.Marshal(m)
	if err != nil {
		fmt.Println("Failed to Marshal response:", err)
		return err
	}

	// fmt.Println(string(bodybytes))

	n := map[string]interface{}{}
	err = json.Unmarshal(bodybytes, &n)
	if err != nil {
		fmt.Println("Failed to Unmarshal the response:", err)
		return err
	}

	for _, v := range n["sessions"].([]interface{}) { // use type assertion to loop over []interface{}
		bodybytes1, err := json.Marshal(v)
		if err != nil {
			fmt.Println("Failed to Marshal response-data:", err)
			return err
		}
		k := map[string]interface{}{}
		err = json.Unmarshal(bodybytes1, &k)
		if err != nil {
			fmt.Println("Failed to Unmarshal response-data:", err)
			return err
		}
		// fmt.Println(string(bodybytes1))
		av := k["available_capacity"].(float64)
		dose1 := k["available_capacity_dose1"].(float64)
		age := k["min_age_limit"].(float64)

		if !(av > 0 && dose1 > 0 && age == float64(18)) {

			name := k["name"].(string)
			addr := k["address"].(string)

			msg := "Dear Ayush Don,\n\nCongratulations :) , vaccine slot is available in your pin 842001.\n\n	**Address**\n		" + name + "\n		" + addr + "\n\n\nThanks\nAyush Salampuriya"
			client, err := sms.CreateClient("smtp.gmail.com", 587, emailSettings.SourceEmail, emailSettings.SourceEmailPassword)
			if err != nil {
				log.Fatal(err)
			}

			//note: if in github.com/acmacalister/sms.yml, there is not entry of "gmail-in", then add a new entry:- gmail-in: "@gmail.com"
			if err := client.Deliver(emailSettings.DestinationEmail, "gmail-in", msg); err != nil {
				log.Fatal(err)
			}
		}

	}

	return nil

}
