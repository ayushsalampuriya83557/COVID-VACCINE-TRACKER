package main

import (
	"COVID-VACCINE-TRACKER/api"
	"log"
	"time"

	"github.com/robfig/cron"
)


func main() {
	//covid vaccine alert app
	// _ = api.FindAvailableSlots()
	// go cronjob()
	log.Println("\n______________________________________________________________________________________\n\n***************Added cron for checking alert.....****************\n______________________________________________________________________________________\n.")
	for true {
		_ = api.FindAvailableSlots()
		time.Sleep(time.Second * 30)
	}

}

func cronjob() {
	c := cron.New()
	c.AddFunc("* * * * * * *", func() {
		_ = api.FindAvailableSlots()
	})

	log.Println("\n______________________________________________________________________________________\n\n***************Added cron for checking alert.....****************\n______________________________________________________________________________________\n.")
	c.Start()
}