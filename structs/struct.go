package structs

type AllUrl struct {
	ServerUrl         string `toml:"apiServerURL"`
	FindByPinUrl      string `toml:"findByPin"`
	FindByDistrictUrl string `toml:"findByDistrict"`
}

type Config struct {
	AllUrls     AllUrl      `toml:"Urls"`
	EmailConfig EmailConfig `toml:"EmailSettings"`
}

type EmailConfig struct {
	SourceEmail         string `toml:"sendFrom"`
	DestinationEmail    string `toml:"sendTo"`
	SourceEmailPassword string `toml:"passwordOfSourceEmail"`
}
